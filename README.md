Vrijeme mjerenja CHRONO-a:


Vrijeme ucitavanja filea: 14738.1 ms

Pretrage:
Vrijeme pretrage po kljucu: 0.6506 ms
Vrijeme pretrage N zapisa po kljucu: 0.4938 ms
Vrijeme pretrage po vrijednosti: 398.124 ms
Vrijeme pretrage N zapisa po vrijednosti: 1.7815 ms

Brisanja:
Vrijeme brisanja po kljucu: 0.1869 ms
Vrijeme brisanja N zapisa po kljucu: 0.2809 ms
Vrijeme brisanja po vrijednosti: 0.1723 ms
Vrijeme brisanja N zapisa po vrijednosti: 400.849 ms

Dohvacanja:
Vrijeme dohvacanja najvece vrijednosti: 0.2159 ms
Vrijeme dohvacanja najmanje vrijednosti: 0.2753 ms
Vrijeme dohvacanja N najvecih vrijednosti: 544.495 ms
Vrijeme dohvacanja N najmanjih vrijednosti: 558.03 ms

Dodavanja:
Vrijeme dodavanja N zapisa po kljucu: 0.3559 ms
Vrijeme dodavanja zapisa po kljucu: 0.283 ms
Vrijeme dodavanja N zapisa po vrijednosti: 200.145 ms


------------------------------------------------------
ZAKLJUČAK:
Priloženi kod koristi sljedeće strukture podataka: vektore, unordered_map(hash) te prioritetni red (min_heap i max_heap)
Vremena su relativno zadovoljavajuća uzeći u obzir količinu podataka.

"unoMap" u kodu je spremnik koji koristi strings (redke) kao ključeve te vektore sa vrijednostima redaka u obliku stringa kao vrijednost ključa.

Unordered_map se koristi prilikom pretraživanja, brisanja i dodavanja - složenost O(c)
Prioritetni red se koristi za dohvaćanje maksimalnih i minimalnih vrijednosti - složenost O(n log(n))

