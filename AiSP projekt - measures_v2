#include <iostream>
#include <vector>
#include <unordered_map>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <queue>
#include <chrono>

using std::vector;
using std::unordered_map;
using std::string;


class bazaPodataka {
public:
    void ucitajPodatke(const string& filename) {
        std::ifstream file(filename);
        string header;
        std::getline(file, header);
        while (std::getline(file, header)) {
            auto redak = splitLine(header);
            bazaPodataka.push_back(std::vector<string>{redak[0], redak[1], redak[12]});
            unoMap[std::to_string(bazaPodataka.size())] = bazaPodataka.back();
            minHeap.push(redak[0]);
            maxHeap.push(redak[0]);
            minHeap.push(redak[1]);
            maxHeap.push(redak[1]);
            minHeap.push(redak[12]);
            maxHeap.push(redak[12]);
        }
        file.close();
    }



    void pretraziPoKljucu(const string &kljuc) const {
        auto start = std::chrono::steady_clock::now();

        auto it = unoMap.find(kljuc);
        if (it != unoMap.end()) {
            const auto &redak = it->second;
            std::cout << "Zapis na retku " << kljuc << ": ";
            for (const auto &vrijednost : redak) {
                std::cout << vrijednost << ", ";
            }
            std::cout << std::endl;
        } else {
            std::cout << "Zapis za redak ne postoji." << std::endl;
        }

        auto end = std::chrono::steady_clock::now();
        std::chrono::duration<double, std::milli> vrijeme = end - start;
        std::cout << "Vrijeme pretrage po kljucu: " << vrijeme.count() << " ms" << std::endl;
    }



    void pretraziNPoKljucu(const string &kljuc, int n) const {
        auto start = std::chrono::steady_clock::now();

        if (n <= 0 || n > 3) {
            std::cout << "n mora biti između 0 i 3" << std::endl;
            return;
        }
        auto it = unoMap.find(kljuc);
        if(it != unoMap.end()){
            const auto &redak = it->second;
            std::cout << n << " zapisa za kljuc: ";
            int brojac = 0;
            for (const auto &vrijednost : redak) {
                std::cout << vrijednost << ", ";
                if (++brojac == n) {
                    break;
                }
            }
            std::cout << std::endl;
        } else {
            std::cout << "Zapisi ne postoje." << std::endl;
        }

        auto end = std::chrono::steady_clock::now();
        std::chrono::duration<double, std::milli> vrijeme = end - start;
        std::cout << "Vrijeme pretrage N zapisa po kljucu: " << vrijeme.count() << " ms" << std::endl;
    }



    void pretraziPoVrijednosti(const string &vrijednost) const {
        auto start = std::chrono::steady_clock::now();

        int brojac = 0;
        for (const auto &unos : unoMap) {
            const auto &redak = unos.second;
            auto it = std::find(redak.begin(), redak.end(), vrijednost);
            if (it != redak.end()) {
                std::cout << "Zapis pronaden u retku " << unos.first << ": ";
                // Ispis prve tri vrijednosti u retku
                int ispisBrojac = 0;
                for (const auto &vrijednost : redak) {
                    std::cout << vrijednost << ", ";
                    if (++ispisBrojac == 3) {
                        break;
                    }
                }
                std::cout << std::endl;
                brojac++;
                if (brojac >= unoMap.size()) {
                    break;
                }
            }
        }
        if (brojac == 0) {
            std::cout << "Trazena vrijednost nije pronadena." << std::endl;
        }

        auto end = std::chrono::steady_clock::now();
        std::chrono::duration<double, std::milli> vrijeme = end - start;
        std::cout << "Vrijeme pretrage po vrijednosti: " << vrijeme.count() << " ms" << std::endl;
    }



    void pretraziNVrijednosti(const string &vrijednost, int n) const {
        auto start = std::chrono::steady_clock::now();

        int brojac = 0;
        for (const auto &unos : unoMap) {
            const auto &redak = unos.second;
            auto it = std::find(redak.begin(), redak.end(), vrijednost);
            if (it != redak.end()) {
                std::cout << "Vrijednost " << vrijednost << " pronadena u retku: " << unos.first << std::endl;
                brojac++;
                if (brojac >= n) {
                    break;
                }
            }
        }
        if (brojac == 0) {
            std::cout << "Vrijednost nije pronadena." << std::endl;
        }

        auto end = std::chrono::steady_clock::now();
        std::chrono::duration<double, std::milli> vrijeme = end - start;
        std::cout << "Vrijeme pretrage N zapisa po vrijednosti: " << vrijeme.count() << " ms" << std::endl;
    }



    void obrisiPoKljucu(const string &key, int stupac) {
        auto start = std::chrono::steady_clock::now();

        auto it = unoMap.find(key);
        if (it != unoMap.end()) {
            auto &redak = it->second;
            if (stupac >= 1 && stupac <= redak.size()) {
                redak.erase(redak.begin() + stupac - 1);
                std::cout << "Zapis je obrisan." << std::endl;
            } else {
                std::cout << "Broj stupaca se ne podudara s bazom." << std::endl;
            }
        } else {
            std::cout << "Zapis ne postoji." << std::endl;
        }

        auto end = std::chrono::steady_clock::now();
        std::chrono::duration<double, std::milli> vrijeme = end - start;
        std::cout << "Vrijeme brisanja po kljucu: " << vrijeme.count() << " ms" << std::endl;
    }



    void obrisiNPoKljucu(const string &key, int n) {
        auto start = std::chrono::steady_clock::now();

        auto it = unoMap.find(key);
        if (it != unoMap.end()) {
            auto &redak = it->second;
            if (n >= 1 && n <= redak.size()) {
                redak.erase(redak.begin(), redak.begin() + n);
                std::cout << n << " zapisa obrisano." << std::endl;
            } else {
                std::cout << "Broj stupaca se ne podudara s bazom." << std::endl;
            }
        } else {
            std::cout << "Zapis ne postoji." << std::endl;
        }

        auto end = std::chrono::steady_clock::now();
        std::chrono::duration<double, std::milli> vrijeme = end - start;
        std::cout << "Vrijeme brisanja N zapisa po kljucu: " << vrijeme.count() << " ms" << std::endl;
    }




    void obrisiPoVrijednosti(const string &key, const string &vrijednost) {
        auto start = std::chrono::steady_clock::now();

        auto it = unoMap.find(key);
        if (it != unoMap.end()) {
            auto &redak = it->second;
            auto it2 = std::remove(redak.begin(), redak.end(), vrijednost);
            redak.erase(it2, redak.end());
            std::cout << "Zapisi obrisan" << std::endl;
        } else {
            std::cout << "Zapis ne postoji." << std::endl;
        }

        auto end = std::chrono::steady_clock::now();
        std::chrono::duration<double, std::milli> vrijeme = end - start;
        std::cout << "Vrijeme brisanja po vrijednosti: " << vrijeme.count() << " ms" << std::endl;
    }



    void obrisiNPoVrijednosti(const string &vrijednost, int n) {
        auto start = std::chrono::steady_clock::now();

        int brojacBrisanja = 0;
        for (auto &unos : unoMap) {
            auto &redak = unos.second;
            auto it = std::find(redak.begin(), redak.end(), vrijednost);
            if (it != redak.end()) {
                redak.erase(it);
                brojacBrisanja++;
                if (brojacBrisanja == n) {
                    break;
                }
            }
        }

        auto end = std::chrono::steady_clock::now();
        std::chrono::duration<double, std::milli> vrijeme = end - start;
        std::cout << "Vrijeme brisanja N zapisa po vrijednosti: " << vrijeme.count() << " ms" << std::endl;

        std::cout << brojacBrisanja << "Zapisi su obrisani." << std::endl;
    }



    void ispisiNajvecuVrijednost() const {
        auto start = std::chrono::steady_clock::now();

        if (!maxHeap.empty()) {
            std::cout << "Najveca vrijednost: " << maxHeap.top() << std::endl;
        } else {
            std::cout << "Max heap ne sadrzava zapise." << std::endl;
        }

        auto end = std::chrono::steady_clock::now();
        std::chrono::duration<double, std::milli> vrijeme = end - start;
        std::cout << "Vrijeme dohvacanja najvece vrijednosti: " << vrijeme.count() << " ms" << std::endl;
    }



    void ispisiNajmanjuVrijednost() const {
        auto start = std::chrono::steady_clock::now();

        if (!minHeap.empty()) {
            std::cout << "Najmanja vrijednost: " << minHeap.top() << std::endl;
        } else {
            std::cout << "Min heap ne sadrzava zapise.." << std::endl;
        }

        auto end = std::chrono::steady_clock::now();
        std::chrono::duration<double, std::milli> vrijeme = end - start;
        std::cout << "Vrijeme dohvacanja najmanje vrijednosti: " << vrijeme.count() << " ms" << std::endl;
    }



    void ispisiNNajvecihVrijednosti(int n) const {
        auto start = std::chrono::steady_clock::now();

        if (n <= 0) {
            std::cout << "N mora biti pozitivan broj." << std::endl;
            return;
        }
        if (maxHeap.empty()) {
            std::cout << "Max heap ne sadrzava zapise." << std::endl;
            return;
        }
        std::priority_queue<std::string, std::vector<std::string>, std::less<std::string>> maxHeap2 = maxHeap;
        std::cout << n << " najvecih vrijednosti: " << std::endl;
        for (int i = 0; i < n; ++i) {
            if (!maxHeap2.empty()) {
                std::cout << maxHeap2.top() << " ";
                maxHeap2.pop();
            }
        }

        auto end = std::chrono::steady_clock::now();
        std::chrono::duration<double, std::milli> vrijeme = end - start;
        std::cout << "Vrijeme dohvacanja N najvecih vrijednosti: " << vrijeme.count() << " ms" << std::endl;
    }



    void ispisiNNajmanjihVrijednosti(int n) const {
        auto start = std::chrono::steady_clock::now();

        if (n <= 0) {
            std::cout << "n mora biti pozitivan." << std::endl;
            return;
        }
        if (minHeap.empty()) {
            std::cout << "Min heap ne sadrzava zapise." << std::endl;
            return;
        }
        std::priority_queue<std::string, std::vector<std::string>, std::greater<std::string>> minHeap2 = minHeap;
        std::cout << n << " najmanje vrijednosti: " << std::endl;
        for (int i = 0; i < n; ++i) {
            if (!minHeap2.empty()) {
                std::cout << minHeap2.top() << " ";
                minHeap2.pop();
            }
        }

        auto end = std::chrono::steady_clock::now();
        std::chrono::duration<double, std::milli> vrijeme = end - start;
        std::cout << "Vrijeme dohvacanja N najmanjih vrijednosti: " << vrijeme.count() << " ms" << std::endl;
    }



    void dodajZapisPoKljucu(const string &key, const std::vector<string> &values) {
        auto start = std::chrono::steady_clock::now();

        auto it = unoMap.find(key);
        if (it != unoMap.end()) {
            std::cout << "Zapis za kljuc " << key << " već postoji. Koristite azuriranje ako je potrebno." << std::endl;
        } else {
            unoMap[key] = values;
            bazaPodataka.push_back(values);

            for (const auto &value : values) {
                minHeap.push(value);
                maxHeap.push(value);
            }
            std::cout << "Novi zapis dodan za kljuc " << key << std::endl;
        }

        auto end = std::chrono::steady_clock::now();
        std::chrono::duration<double, std::milli> vrijeme = end - start;
        std::cout << "Vrijeme dodavanja zapisa po kljucu: " << vrijeme.count() << " ms" << std::endl;
    }



    void dodajNZapisaPoKljucu(const string &key, const vector<std::vector<string>> &records) {
        auto start = std::chrono::steady_clock::now();

        auto it = unoMap.find(key);
        if (it != unoMap.end()) {
            std::cout << "Zapis za kljuc " << key << " vec postoji." << std::endl;
        } else {
            for (const auto &record : records) {
                unoMap[key] = record;
                bazaPodataka.push_back(record);
            }
            std::cout << records.size() << " novih zapisa dodano za kljuc " << key << std::endl;
        }

        auto end = std::chrono::steady_clock::now();
        std::chrono::duration<double, std::milli> vrijeme = end - start;
        std::cout << "Vrijeme dodavanja N zapisa po kljucu: " << vrijeme.count() << " ms" << std::endl;
    }




    void dodajZapisPoVrijednosti(const string &value, const std::vector<string> &record) {
        auto start = std::chrono::steady_clock::now();

        auto it = std::find_if(bazaPodataka.begin(), bazaPodataka.end(), [&](const auto &row) {
            return std::find(row.begin(), row.end(), value) != row.end();
        });
        if (it != bazaPodataka.end()) {
            unoMap[std::to_string(bazaPodataka.size() + 1)] = record;
            bazaPodataka.push_back(record);
            std::cout << "Novi zapis dodan za vrijednost " << value << std::endl;
        } else {
            std::cout << "Vrijednost " << value << " nije pronadena u postojecim zapisima." << std::endl;
        }

        auto end = std::chrono::steady_clock::now();
        std::chrono::duration<double, std::milli> vrijeme = end - start;
        std::cout << "Vrijeme dodavanja N zapisa po vrijednosti: " << vrijeme.count() << " ms" << std::endl;
    }


private:
    vector<std::vector<string>> bazaPodataka;
    unordered_map<string, std::vector<string>> unoMap;

    std::priority_queue<std::string, std::vector<std::string>, std::greater<std::string>> minHeap;

    std::priority_queue<std::string, std::vector<std::string>, std::less<std::string>> maxHeap;


    vector<string> splitLine(const string &line) const {
        vector<string> redak;
        std::stringstream ss(line);
        string cell;
        while (std::getline(ss, cell, ',')) {
            redak.emplace_back(cell);
        }
        return redak;
    }
};

int main() {
    bazaPodataka bazaPodataka;
    auto start_ucitajPodatke = std::chrono::steady_clock::now();
    bazaPodataka.ucitajPodatke("C:/Users/Tomo/CLionProjects/untitled/measures_v2.csv");
    auto end_ucitajPodatke = std::chrono::steady_clock::now();
    std::chrono::duration<double, std::milli> vrijeme = end_ucitajPodatke - start_ucitajPodatke;
    std::cout << "Vrijeme ucitavanja filea: " << vrijeme.count() << " ms" << std::endl;


    bazaPodataka.pretraziPoKljucu("500000");

    bazaPodataka.pretraziNPoKljucu("4", 3);

    bazaPodataka.pretraziPoVrijednosti("-0.886074423789978");

    bazaPodataka.pretraziNVrijednosti("71", 5);

    bazaPodataka.obrisiPoKljucu("2", 2);
    bazaPodataka.pretraziPoKljucu("2");

    bazaPodataka.obrisiNPoKljucu("3", 2);
    bazaPodataka.pretraziPoKljucu("3");

    bazaPodataka.obrisiPoVrijednosti("4", "19.083030700683594");
    bazaPodataka.pretraziPoKljucu("4");

    bazaPodataka.obrisiNPoVrijednosti("19.850690841674805", 500000);
    bazaPodataka.pretraziNVrijednosti("19.850690841674805", 3);

    bazaPodataka.ispisiNajvecuVrijednost();
    bazaPodataka.ispisiNajmanjuVrijednost();

    bazaPodataka.ispisiNNajvecihVrijednosti(5);
    bazaPodataka.ispisiNNajmanjihVrijednosti(5);


    std::vector<std::vector<std::string>> noviZapisi = {
            {"8888", "7575", "500000"},
            {"1231", "6666", "2352"}
    };
    bazaPodataka.dodajNZapisaPoKljucu("1500001", noviZapisi);
    bazaPodataka.pretraziNPoKljucu("1500001", 2);

    std::vector<std::string> noviZapis = {"1111", "2222", "3333"};
    bazaPodataka.dodajZapisPoKljucu("1500000", noviZapis);
    bazaPodataka.pretraziPoKljucu("1500000");

    std::vector<std::string> noviZapisPoVrijednosti = {"10000000"};
    bazaPodataka.dodajZapisPoVrijednosti("30.72129992113748", noviZapisPoVrijednosti);

    return 0;
}
